#include <ClusterUncertaintyTool/ClusterUncertaintyTool.h>
#include <ClusterUncertaintyTool/JetMaker.h>
#include <ClusterUncertaintyTool/JetMakerWithSystematics.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class ClusterUncertaintyTool+;
#pragma link C++ class JetMaker+;
#pragma link C++ class JetMakerWithSystematics+;
#endif
