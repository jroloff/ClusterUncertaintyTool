#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODEventInfo/EventInfo.h"


#include "JetRec/PseudoJetGetter.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetRec/JetFinder.h"
#include "JetRec/JetSplitter.h"
#include "JetRec/JetRecTool.h"
#include "JetRec/JetDumper.h"
#include "JetRec/JetToolRunner.h"

#include "AsgTools/Check.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetInterface/IJetFromPseudojet.h"
#include "xAODJet/Jet.h"
#include "JetEDM/PseudoJetVector.h"
#include "JetEDM/JetConstituentFiller.h"
#include "JetEDM/FastJetLink.h"
#include "xAODJet/Jet_PseudoJet.icc"
#include "JetRec/JetFromPseudojet.h"
#include "JetEDM/IndexedConstituentUserInfo.h"
#include "JetEDM/LabelIndex.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "AthLinks/ElementLink.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"

#include "xAODAnaHelpers/HelperFunctions.h"
#include <xAODAnaHelpers/tools/ReturnCheck.h>
#include "ClusterUncertaintyTool/JetMakerWithSystematics.h"


#include <sstream>
#include <string>

using namespace std;


typedef IJetFromPseudojet::NameList NameList;

ClassImp(JetMakerWithSystematics)


JetMakerWithSystematics::JetMakerWithSystematics():
	Algorithm("JetMakerWithSystematics"),
	m_clust(0)
{
	m_name = "JetMakerWithSystematics";
	m_jetalg = fastjet::cambridge_algorithm;	
	m_jetR = 1.0;	
	m_inContainerName = "";
 	m_outContainerName = m_name;
}

EL::StatusCode JetMakerWithSystematics :: setupJob (EL::Job& job){
  Info("setupJob()", "Calling setupJob");

  job.useXAOD ();
  xAOD::Init("JetMakerWithSystematics").ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerWithSystematics :: histInitialize (){
  RETURN_CHECK("xAH::Algorithm::algInitialize()", xAH::Algorithm::algInitialize(), "");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerWithSystematics :: fileExecute (){
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerWithSystematics :: changeInput (bool /*firstFile*/){
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode JetMakerWithSystematics::initialize(){
  m_event = wk()->xaodEvent();
	m_store = wk()->xaodStore();
  const xAOD::EventInfo* eventInfo(nullptr);
  RETURN_CHECK("JetCalibrator::execute()", HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store, m_verbose) ,"");

  SkipNegativeEnergy = true;
  Label = "LCTopo";
  JetAlgorithm = "AntiKt";
  PtMin =  0.1;
  InputLabel = "LCTopo";
  m_PseudoJetGetter = "lcget";

  m_systematics.push_back("");
  m_systematics.push_back("CAR_");
  m_systematics.push_back( "CE_");
  m_systematics.push_back( "CER_");
  m_systematics.push_back( "CMergeHadEM_");
  m_systematics.push_back( "CMergeEM_");
  m_systematics.push_back( "Csplit_");
  for(int i=0; i<9; i++){
    m_systematics.push_back( Form("CESUp%d_", i));
    m_systematics.push_back( Form("CESDown%d_", i));
  }

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  for(unsigned int i=0; i<m_systematics.size(); i++){
    ToolHandleArray<IPseudoJetGetter> hgetsTmp;
    plcget = new PseudoJetGetter(Form("m_pjgetter_%d", i));
    std::string inputcont = Form("%s%s", m_systematics[i].c_str(), InputClusters.c_str());
    std::string outcont = Form("%s_%sPJGetter", m_systematics[i].c_str(), m_outContainerName.c_str());
    EL_RETURN_CHECK("initialize()", plcget->setProperty("InputContainer", inputcont.c_str()) );
    EL_RETURN_CHECK("initialize()", plcget->setProperty("OutputContainer", outcont.c_str() ) );
    EL_RETURN_CHECK("initialize()", plcget->setProperty("Label", InputLabel.c_str()) );
    EL_RETURN_CHECK("initialize()", plcget->setProperty("SkipNegativeEnergy", true) );
    EL_RETURN_CHECK("initialize()", plcget->setProperty("GhostScale", 0.0) );
    EL_RETURN_CHECK("initialize()", plcget->initialize() );
    ToolHandle<IPseudoJetGetter> hlcget(plcget);
    hgetsTmp.push_back(hlcget);
    hgets.push_back(hgetsTmp);
  }

  pbuild = new JetFromPseudojet("jetbuild");
  ToolHandle<IJetFromPseudojet> hbuild(pbuild);
  vector<string> jetbuildatts;
  jetbuildatts.push_back("ActiveArea");
  jetbuildatts.push_back("ActiveAreaFourVector");
  EL_RETURN_CHECK("initialize()", pbuild->setProperty("Attributes", jetbuildatts) );
  EL_RETURN_CHECK("initialize()", pbuild->initialize() );

  pfind = new JetFinder("jetfind");
  EL_RETURN_CHECK("initialize()", pfind->setProperty("JetAlgorithm", JetAlgorithm.c_str()) );
  EL_RETURN_CHECK("initialize()", pfind->setProperty("JetRadius", JetRadius));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("PtMin", PtMin));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("GhostArea", 0.01));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("RandomOption", 1));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("JetBuilder", hbuild));
  ToolHandle<IJetFinder> hfind(pfind);
  EL_RETURN_CHECK("initialize()",pfind->initialize() );

  for(int i=0; i<m_systematics.size(); i++){
    std::string outcont = Form("%s%s", m_systematics[i].c_str(), OutputContainer.c_str());
    std::string inputcont = Form("%s%s", m_systematics[i].c_str(), InputClusters.c_str());

    pjrf = new JetRecTool(Form("jrfind_%d", i));
    ToolHandleArray<IJetExecuteTool> hrecs;
    std::cout << "Writing jet collection: " << outcont << std::endl;
    EL_RETURN_CHECK("initialize()", pjrf->setProperty("OutputContainer", outcont.c_str()));
    EL_RETURN_CHECK("initialize()", pjrf->setProperty("InputContainer", inputcont.c_str()));
    EL_RETURN_CHECK("initialize()", pjrf->setProperty("PseudoJetGetters", hgets[i]));
    EL_RETURN_CHECK("initialize()", pjrf->setProperty("JetFinder", hfind));
    EL_RETURN_CHECK("initialize()", pjrf->initialize());
    hrecs.push_back(pjrf);

    JetToolRunner* jrunTmp = new JetToolRunner(Form("jetrunner_%d", i));
    EL_RETURN_CHECK("initialize()", jrunTmp->setProperty("Tools", hrecs) );
    EL_RETURN_CHECK("initialize()", jrunTmp->initialize() );
    jrun.push_back(jrunTmp);
  }


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerWithSystematics::execute() {
	m_numEvent++;
  for(unsigned int i=0; i<m_systematics.size(); i++){
    jrun[i]->execute();
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerWithSystematics :: postExecute (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerWithSystematics :: finalize (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerWithSystematics :: histFinalize (){

  Info("histFinalize()", "Calling histFinalize");
  RETURN_CHECK("xAH::Algorithm::algFinalize()", xAH::Algorithm::algFinalize(), "");
  return EL::StatusCode::SUCCESS;
}






