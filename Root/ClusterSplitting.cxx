#include <vector>

#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "xAODCore/ShallowCopy.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowAuxContainer.h"

#include "TRandom3.h"

#include "ClusterUncertaintyTool/ClusterSplitting.h"



using namespace std;


ClusterSplitting::ClusterSplitting(const std::string& name) : JetConstituentModifierBase(name)
                                                                      
{

#ifdef ASG_TOOL_ATHENA
  declareInterface<IJetConstituentModifier>(this);
#endif

  myrand_global = new TRandom3(0); //for uncerts.
}


StatusCode ClusterSplitting::process(xAOD::CaloClusterContainer* cont) const {
  xAOD::CaloClusterContainer* newConst = new xAOD::CaloClusterContainer();
  for(xAOD::CaloCluster* cl : *cont) {
    float EMProb = cl->auxdata<float>("EM_PROBABILITY");
    float centerMag = cl->auxdata<float>("CENTER_MAG");
    float secondR = cl->auxdata<float>("SECOND_R");

    std::vector<float> i_EMax = cl->auxdata<std::vector<float> >("e_sampl");

    double maxE = 0;
    double maxE_ind = -1;
    for(unsigned int k=0; k<i_EMax.size(); k++){
      if(i_EMax[k] > maxE){
        maxE = i_EMax[k];
        maxE_ind = k;
      }
    }
    int i = maxE_ind;

    double sigmaI = atan(sqrt(secondR)/centerMag)*cosh(cl->eta());

    if (myrand_global->Uniform(0., 1.) < 0.2 && EMProb < 0.5 && (i_EMax[i] == 2 || i_EMax[i] == 6)){
      fastjet::PseudoJet split_const1();
      split_const1.reset_PtYPhiM(cl->pt() / 2., cl->eta(), cl->phi() + sigmaI / 2., 0.);
      fastjet::PseudoJet split_const2();
      split_const2.reset_PtYPhiM(cl->pt() / 2., cl->eta(), cl->phi() - sigmaI / 2., 0.);
      xAOD::CaloCluster * cl2 = new xAOD::CaloCluster(*cl);
      cl->setEta(split_const1.eta());
      cl->setE(split_const1.e());
      cl->setPhi(split_const1.phi());
      
      cl2->setEta(split_const2.eta());
      cl2->setE(split_const2.e());
      cl2->setPhi(split_const2.phi());
      
      newConst->push_back(cl2); 
    }
  }

  for(xAOD::CaloCluster* cl : *newConst) {
    cont->push_back(cl);
  }

  return StatusCode::SUCCESS;
}

StatusCode ClusterSplitting::process(xAOD::IParticleContainer* cont) const {
  xAOD::CaloClusterContainer* clust = dynamic_cast<xAOD::CaloClusterContainer*> (cont); // Get CaloCluster container
  if(clust) return process(clust);
  return StatusCode::FAILURE;
}






