#include <vector>
#include "fastjet/PseudoJet.hh"
#include "xAODCore/ShallowCopy.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "TH2.h"
#include "TFile.h"
#include "TRandom3.h"
#include "ClusterUncertaintyTool/CES.h"


using namespace std;

ClusterEnergyScale::ClusterEnergyScale(const std::string& name) : JetConstituentModifierBase(name)  {

#ifdef ASG_TOOL_ATHENA
  declareInterface<IJetConstituentModifier>(this);
#endif
  declareProperty("Method", m_method = "");
  declareProperty("Uncertainty", m_type );
  myrand_global = new TRandom3(0); //for uncerts.
}


StatusCode ClusterEnergyScale::process(xAOD::CaloClusterContainer* cont) const {
	// Need to double check if this is done per-jet, per-event, etc.
  double CES_option2_factors[7]={myrand_global->Gaus(0,1),myrand_global->Gaus(0,1),myrand_global->Gaus(0,1),myrand_global->Gaus(0,1),myrand_global->Gaus(0,1),myrand_global->Gaus(0,1),myrand_global->Gaus(0,1)};

	vector<double> etabins =  {0, 1.1,  1.4,  1.5,  1.8,  1.9,  5.0 };
	vector<double> alpha =    {0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
	vector<double> beta =     {500,  500,  500,  0,    500,  0,    500 };

  std::string filename = "$ROOTCOREBIN/data/ClusterUncertaintyTool/cluster_uncert_map.root";
	TFile *clusterfile = new TFile(filename.c_str());
	TH2D *cluster_means = (TH2D*) clusterfile->Get("Mean");
	TH2D *cluster_rmss = (TH2D*) clusterfile->Get("RMS");

  for(xAOD::CaloCluster *cl : *cont) {
		double eta = cl->eta();
		double E = cl->e();
		double p = cl->pt();
		double abs_eta = fabs(eta);

		int etabin = -1;
		for(unsigned int i=0; i<etabins.size(); i++){
			if(fabs(eta) < etabins[i]){
				etabin = i;
			}
		}

    double CESfactor = alpha[etabin]*(1.+beta[etabin]/E);

    int pbin = cluster_means->GetXaxis()->FindBin(p);
    int ebin = cluster_means->GetYaxis()->FindBin(abs_eta);
    if (pbin > cluster_means->GetNbinsX()) pbin = cluster_means->GetNbinsX();
    if (pbin < 1) pbin = 1;
    if (ebin > cluster_means->GetNbinsX()) ebin  = cluster_means->GetNbinsX();
    if (ebin < 1) ebin  = 1;

    double myCES = fabs(cluster_means->GetBinContent(pbin,ebin)-1.);

    if (p > 350) myCES = 0.1;

    switch (m_type){
      case EMFracLow:      myCES = getCES_EMFracLow(cl, myCES);      break;
      case EMFracHigh:     myCES = getCES_EMFracHigh(cl, myCES);     break;
      case EMProb:         myCES = getCES_EMProb(cl, myCES);         break;
      case EtaForward:     myCES = getCES_EtaForward(cl, myCES);     break;
      case EtaCentral:     myCES = getCES_EtaCentral(cl, myCES);     break;
      case OutOfTestRange: myCES = getCES_OutOfTestRange(cl, myCES); break;
      case InTestRange:    myCES = getCES_InTestRange(cl, myCES);    break;
      case PtLow:          myCES = getCES_PtLow(cl, myCES);          break;
      case PtHigh:         myCES = getCES_PtHigh(cl, myCES);         break;
      default: break;

    }

    
	  if(strcmp("Up", m_method.c_str()) == 0) cl->setE( E*(1+myCES));
	  if(strcmp("Down", m_method.c_str()) == 0) cl->setE( E*(1-myCES));
  }

	clusterfile->Close();

	delete clusterfile;

  return StatusCode::SUCCESS;
}

StatusCode ClusterEnergyScale::process(xAOD::IParticleContainer* cont) const{
  xAOD::CaloClusterContainer* clust = dynamic_cast<xAOD::CaloClusterContainer*> (cont); // Get CaloCluster container
  if(clust) return process(clust);
  return StatusCode::FAILURE;
}

double ClusterEnergyScale::getCES_EMFracLow(xAOD::CaloCluster* cluster, double CES) const{
	float emfrac = cluster->auxdata<float>("EM_FRAC");
  if(emfrac > 0.5) return 0;
  return CES;
}

double ClusterEnergyScale::getCES_EMFracHigh(xAOD::CaloCluster* cluster, double CES) const{
	float emfrac = cluster->auxdata<float>("EM_FRAC");
  if(emfrac < 0.5) return 0;
  return CES;
}

double ClusterEnergyScale::getCES_EMProb(xAOD::CaloCluster* cluster, double CES) const{
	float emprob = cluster->auxdata<float>("EM_PROBABILITY");
  if(emprob > 0.9) return 0.005;
  return CES;
}
double ClusterEnergyScale::getCES_OutOfTestRange(xAOD::CaloCluster* cluster, double CES) const{
  if(!( cluster->e() > 30 && fabs(cluster->eta()) > 0.6) ) return 0;
  return CES;
}
double ClusterEnergyScale::getCES_InTestRange(xAOD::CaloCluster* cluster, double CES) const{
  if( cluster->e() > 30 && fabs(cluster->eta()) > 0.6) return 0;
  return CES;
}

double ClusterEnergyScale::getCES_EtaForward(xAOD::CaloCluster* cluster, double CES) const{
  if(cluster->eta() < 1.4) return 0;
  return CES;
}

double ClusterEnergyScale::getCES_EtaCentral(xAOD::CaloCluster* cluster, double CES) const{
  if(cluster->eta() >= 1.4) return 0;
  return CES;
}
double ClusterEnergyScale::getCES_PtLow(xAOD::CaloCluster* cluster, double CES) const{
  if(cluster->pt() >= 2.5) return 0;
  return CES;
}

double ClusterEnergyScale::getCES_PtHigh(xAOD::CaloCluster* cluster, double CES) const{
  if(cluster->pt() < 2.5) return 0;
  return CES;
}


