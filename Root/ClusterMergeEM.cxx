#include <vector>

#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "xAODCore/ShallowCopy.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowAuxContainer.h"

#include "TRandom3.h"

#include "ClusterUncertaintyTool/ClusterMergeEM.h"



using namespace std;


ClusterMergeEM::ClusterMergeEM(const std::string& name) : JetConstituentModifierBase(name)
                                                                      
{

#ifdef ASG_TOOL_ATHENA
  declareInterface<IJetConstituentModifier>(this);
#endif

  myrand_global = new TRandom3(0); //for uncerts.
}


StatusCode ClusterMergeEM::process(xAOD::CaloClusterContainer* cont) const {
  std::vector<bool> already_merged;
  for(xAOD::CaloCluster *cl : *cont) {
    already_merged.push_back(false);
  }

  for(unsigned int i=0; i<cont->size(); i++){
    if (already_merged[i]) continue;
    xAOD::CaloCluster* cl1 = (*cont)[i];
    fastjet::PseudoJet cl1PJ(cl1->pt(), cl1->eta(), cl1->phi(), cl1->e());
    float EMProb1 = cl1->auxdata<float>("EM_PROBABILITY");
    float centerMag1 = cl1->auxdata<float>("CENTER_MAG");
    float secondR1 = cl1->auxdata<float>("SECOND_R");
    std::vector<float> ESample1 = cl1->auxdata<std::vector<float> >("e_sampl");
    int EMaxInd1 = 0;

    for(unsigned int j=i+1; j<cont->size(); j++){
      if (already_merged[j]) continue;
      xAOD::CaloCluster* cl2 = (*cont)[j];
      fastjet::PseudoJet cl2PJ(cl2->pt(), cl2->eta(), cl2->phi(), cl2->e());
      float EMProb2 = cl2->auxdata<float>("EM_PROBABILITY");
      float centerMag2 = cl2->auxdata<float>("CENTER_MAG");
      float secondR2 = cl2->auxdata<float>("SECOND_R");
      std::vector<float> ESample2 = cl2->auxdata<std::vector<float> >("e_sampl");
      //int EMaxInd2 = std::distance(ESample2, std::max_element(ESample2.begin(), ESample2.end()));
      int EMaxInd2 = 0;

      double sigmaI = atan(sqrt(secondR1)/centerMag1)*cosh(cl1->eta());
      double sigmaJ = atan(sqrt(secondR2)/centerMag2)*cosh(cl2->eta());

      if ( cl1PJ.delta_R(cl2PJ) > sigmaI+sigmaJ)
        continue;

      //for any pair of clusters with the maximum energy in EMB2 or EME2 that are not extremely likely to be a photons.
      if ( ((EMProb1 < 0.5 && EMProb2 < 0.5) && ((EMaxInd1 == 2 && (EMaxInd2 == 2) || (EMaxInd1 == 6 && (EMaxInd2 == 6))))) ){
        already_merged[i] = true;
        already_merged[j] = true;

        fastjet::PseudoJet mergedConst = cl1PJ + cl2PJ;
        cl1->setEta(mergedConst.eta());
        cl1->setE(mergedConst.e());
        cl1->setPhi(mergedConst.phi());

        cl2->setE(0.);
      }
    }
  }


  return StatusCode::SUCCESS;
}

StatusCode ClusterMergeEM::process(xAOD::IParticleContainer* cont) const {
  xAOD::CaloClusterContainer* clust = dynamic_cast<xAOD::CaloClusterContainer*> (cont); // Get CaloCluster container
  if(clust) return process(clust);
  return StatusCode::FAILURE;
}






