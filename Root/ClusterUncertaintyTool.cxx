#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODEventInfo/EventInfo.h"


#include "JetRec/PseudoJetGetter.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetRec/JetFinder.h"
#include "JetRec/JetSplitter.h"
#include "JetRec/JetRecTool.h"
#include "JetRec/JetDumper.h"
#include "JetRec/JetToolRunner.h"

#include "AsgTools/Check.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetInterface/IJetFromPseudojet.h"
#include "xAODJet/Jet.h"
#include "JetEDM/PseudoJetVector.h"
#include "JetEDM/JetConstituentFiller.h"
#include "JetEDM/FastJetLink.h"
#include "xAODJet/Jet_PseudoJet.icc"
#include "JetRec/JetFromPseudojet.h"
#include "JetEDM/IndexedConstituentUserInfo.h"
#include "JetEDM/LabelIndex.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "AthLinks/ElementLink.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"

#include "xAODAnaHelpers/HelperFunctions.h"
#include "xAODAnaHelpers/Algorithm.h"
#include <xAODAnaHelpers/tools/ReturnCheck.h>

#include "ClusterUncertaintyTool/ClusterUncertaintyTool.h"
#include "ClusterUncertaintyTool/CE.h"
#include "ClusterUncertaintyTool/CES.h"
#include "ClusterUncertaintyTool/CAR.h"
#include "ClusterUncertaintyTool/ClusterMergeEM.h"
#include "ClusterUncertaintyTool/ClusterMergeHadEM.h"
#include "ClusterUncertaintyTool/ClusterSplitting.h"
#include "ClusterUncertaintyTool/ClusterUncertaintyTool.h"


#include <sstream>
#include <string>
#include "TString.h"

using namespace std;


typedef IJetFromPseudojet::NameList NameList;

ClassImp(ClusterUncertaintyTool)


ClusterUncertaintyTool::ClusterUncertaintyTool():
	xAH::Algorithm("ClusterUncertaintyTool")//,
{
	m_name = "ClusterUncertaintyTool";
	m_inContainerName = "CaloCalTopoClusters";
 	m_outContainerName = m_name;
  m_nCESVariations = 9;
}

std::string ClusterUncertaintyTool::GetNames(){
return "CAR_AntiKt8LCTopoJets";

}

EL::StatusCode ClusterUncertaintyTool :: setupJob (EL::Job& job){
  Info("setupJob()", "Calling setupJob");

  job.useXAOD ();
  xAOD::Init("ClusterUncertaintyTool").ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ClusterUncertaintyTool :: histInitialize (){
  RETURN_CHECK("xAH::Algorithm::algInitialize()", xAH::Algorithm::algInitialize(), "");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ClusterUncertaintyTool :: fileExecute (){
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ClusterUncertaintyTool :: changeInput (bool /*firstFile*/){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ClusterUncertaintyTool::initialize(){
  m_event = wk()->xaodEvent();
	m_store = wk()->xaodStore();
	std::string clusterColl = m_inContainerName;

  for(int i=0; i<m_nCESVariations; i++){
    ToolHandleArray<IJetConstituentModifier> hclusts_cesUpTmp;
    ToolHandleArray<IJetConstituentModifier> hclusts_cesDownTmp;
    std::string cesUpName = Form("cesUp%d",i);
    ClusterEnergyScale* cesUp = new ClusterEnergyScale(cesUpName);
	  cesUp->setProperty("Method", "Up");
  	cesUp->setProperty("Uncertainty", i);
    ToolHandle<IJetConstituentModifier> iCESUp(cesUp);
    hclusts_cesUpTmp.push_back(iCESUp);
    hclusts_cesUp.push_back(hclusts_cesUpTmp);

    std::string cesDownName = Form("cesDown%d",i);
    ClusterEnergyScale* cesDown = new ClusterEnergyScale(cesDownName);
  	cesDown->setProperty("Method", "Down");
  	cesDown->setProperty("Uncertainty", i);
    ToolHandle<IJetConstituentModifier> iCESDown(cesDown);
    hclusts_cesDownTmp.push_back(iCESDown);
    hclusts_cesDown.push_back(hclusts_cesDownTmp);
  }

  ClusterEnergyScale* ces = new ClusterEnergyScale("ces");
  ToolHandle<IJetConstituentModifier> iCES(ces);
  hclusts_ces.push_back(iCES);

  ClusterEfficiency* ce = new ClusterEfficiency("ce");
  ToolHandle<IJetConstituentModifier> iCE(ce);
  hclusts_ce.push_back(iCE);

  ClusterAngularResolution* car = new ClusterAngularResolution("car");
  ToolHandle<IJetConstituentModifier> iCAR(car);
  hclusts_car.push_back(iCAR);

  ClusterMergeHadEM* cmergeHadEM = new ClusterMergeHadEM("cmergeHadEM");
  ToolHandle<IJetConstituentModifier> iCMHE(cmergeHadEM);
  hclusts_cmergeHadEM.push_back(iCMHE);

  ClusterMergeEM* cmergeEM = new ClusterMergeEM("cmergeEM");
  ToolHandle<IJetConstituentModifier> iCME(cmergeEM);
  hclusts_cmergeEM.push_back(iCME);

  ClusterSplitting* csplit = new ClusterSplitting("cSplit");
  ToolHandle<IJetConstituentModifier> iCS(csplit);
  hclusts_csplit.push_back(iCMHE);

  for(int i=0; i<m_nCESVariations; i++){
    JetConstituentModSequence *cesUpModSeqTmp = new JetConstituentModSequence( "CesUpModifSequence");
    std::string outcont1 = Form("CESUp%d_%s", i, clusterColl.c_str());
    cesUpModSeqTmp->setProperty("InputContainer", clusterColl);
    cesUpModSeqTmp->setProperty("OutputContainer", outcont1.c_str());
    cesUpModSeqTmp->setProperty("Modifiers", hclusts_cesUp[i] );
    cesUpModSeqTmp->initialize();
    ToolHandle<IJetExecuteTool> iclustModTool1(cesUpModSeqTmp);
    cesUpModSeq.push_back(cesUpModSeqTmp);

    JetConstituentModSequence *cesDownModSeqTmp = new JetConstituentModSequence( "CesDownModifSequence");
    std::string outcont = Form("CESDown%d_%s", i, clusterColl.c_str());
    cesDownModSeqTmp->setProperty("InputContainer", clusterColl);
    cesDownModSeqTmp->setProperty("OutputContainer", outcont.c_str());
    cesDownModSeqTmp->setProperty("Modifiers", hclusts_cesDown[i] );
    cesDownModSeqTmp->initialize();
    ToolHandle<IJetExecuteTool> iclustModTool2(cesDownModSeqTmp);
    cesDownModSeq.push_back(cesDownModSeqTmp);
  }

  cesModSeq = new JetConstituentModSequence( "CesModifSequence");
  cesModSeq->setProperty("InputContainer", clusterColl);
  cesModSeq->setProperty("OutputContainer", ("CER_"+clusterColl));
  cesModSeq->setProperty("InputType", "CaloCluster");
  cesModSeq->setProperty("Modifiers", hclusts_ces );
  cesModSeq->initialize();
  ToolHandle<IJetExecuteTool> iclustModTool5(cesModSeq);

  ceModSeq = new JetConstituentModSequence( "CeModifSequence");
  ceModSeq->setProperty("InputContainer", clusterColl);
  ceModSeq->setProperty("OutputContainer", ("CE_"+clusterColl));
  ceModSeq->setProperty("InputType", "CaloCluster");
  ceModSeq->setProperty("Modifiers", hclusts_ce );
  ceModSeq->initialize();
  ToolHandle<IJetExecuteTool> iclustModTool4(ceModSeq);

  cmergeHadEMModSeq = new JetConstituentModSequence( "CmergeHadEMSequence");
  cmergeHadEMModSeq->setProperty("InputContainer", clusterColl);
  cmergeHadEMModSeq->setProperty("OutputContainer", ("CMergeHadEM_"+clusterColl));
  cmergeHadEMModSeq->setProperty("InputType", "CaloCluster");
  cmergeHadEMModSeq->setProperty("Modifiers", hclusts_cmergeHadEM );
  cmergeHadEMModSeq->initialize();
  ToolHandle<IJetExecuteTool> iclustModTool8(cmergeHadEMModSeq);

  cmergeEMModSeq = new JetConstituentModSequence( "CmergeEMSequence");
  cmergeEMModSeq->setProperty("InputContainer", clusterColl);
  cmergeEMModSeq->setProperty("OutputContainer", ("CMergeEM_"+clusterColl));
  cmergeEMModSeq->setProperty("InputType", "CaloCluster");
  cmergeEMModSeq->setProperty("Modifiers", hclusts_cmergeEM );
  cmergeEMModSeq->initialize();
  ToolHandle<IJetExecuteTool> iclustModTool6(cmergeEMModSeq);

  csplitModSeq = new JetConstituentModSequence( "CsplitSequence");
  csplitModSeq->setProperty("InputContainer", clusterColl);
  csplitModSeq->setProperty("OutputContainer", ("Csplit_"+clusterColl));
  csplitModSeq->setProperty("InputType", "CaloCluster");
  csplitModSeq->setProperty("Modifiers", hclusts_csplit );
  csplitModSeq->initialize();
  ToolHandle<IJetExecuteTool> iclustModTool7(csplitModSeq);

  carModSeq = new JetConstituentModSequence( "CarModifSequence");
  carModSeq->setProperty("InputContainer", clusterColl);
  carModSeq->setProperty("OutputContainer", ("CAR_"+clusterColl));
  carModSeq->setProperty("Modifiers", hclusts_car );
  carModSeq->initialize();
  ToolHandle<IJetExecuteTool> iclustModTool3(carModSeq);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ClusterUncertaintyTool::execute() {
 carModSeq->execute();
 ceModSeq->execute();
 cmergeHadEMModSeq->execute();
 cmergeEMModSeq->execute();
 csplitModSeq->execute();

 for(int i=0; i<m_nCESVariations; i++){
   cesDownModSeq[i]->execute();
   cesUpModSeq[i]->execute();
 }
 cesModSeq->execute();


  return EL::StatusCode::SUCCESS;
}




EL::StatusCode ClusterUncertaintyTool :: postExecute (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ClusterUncertaintyTool :: finalize (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ClusterUncertaintyTool :: histFinalize (){
  return EL::StatusCode::SUCCESS;
}






