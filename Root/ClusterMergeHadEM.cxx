#include <vector>

#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "xAODCore/ShallowCopy.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowAuxContainer.h"

#include "TRandom3.h"

#include "ClusterUncertaintyTool/ClusterMergeHadEM.h"



using namespace std;


ClusterMergeHadEM::ClusterMergeHadEM(const std::string& name) : JetConstituentModifierBase(name)
                                                                      
{

#ifdef ASG_TOOL_ATHENA
  declareInterface<IJetConstituentModifier>(this);
#endif

  myrand_global = new TRandom3(0); //for uncerts.
}


StatusCode ClusterMergeHadEM::process(xAOD::CaloClusterContainer* cont) const {

  std::vector<bool> already_merged;
  for(xAOD::CaloCluster* cl : *cont) {
    already_merged.push_back(false);
  }

  for(unsigned int i=0; i<cont->size(); i++){

    if (already_merged[i]) continue;
    xAOD::CaloCluster* cl1 = (*cont)[i];
    fastjet::PseudoJet cl1PJ(cl1->pt(), cl1->eta(), cl1->phi(), cl1->e());
    float EMProb1 = cl1->auxdata<float>("EM_PROBABILITY");
    float centerMag1 = cl1->auxdata<float>("CENTER_MAG");
    float secondR1 = cl1->auxdata<float>("SECOND_R");


    std::vector<float> ESample1 = cl1->auxdata<std::vector<float> >("e_sampl");
    float e_EMB3_1   = ESample1[3];
    float e_HEC0_1   = ESample1[8];
    float e_Tile0_1  = ESample1[12];

    for(unsigned int j=i+1; j<cont->size(); j++){
      if (already_merged[j]) continue;
      xAOD::CaloCluster* cl2 = (*cont)[j];
      fastjet::PseudoJet cl2PJ(cl2->pt(), cl2->eta(), cl2->phi(), cl2->e());
      float EMProb2 = cl2->auxdata<float>("EM_PROBABILITY");
      float centerMag2 = cl2->auxdata<float>("CENTER_MAG");
      float secondR2 = cl2->auxdata<float>("SECOND_R");
      std::vector<float> ESample2 = cl2->auxdata<std::vector<float> >("e_sampl");
      float e_EMB3_2   = ESample2[3];
      float e_HEC0_2   = ESample2[8];
      float e_Tile0_2  = ESample2[12];

      float e_PreSamplerB_1     = ESample1[0];
      float e_EMB1_1   = ESample1[1];
      float e_EMB2_1   = ESample1[2];
      float e_PreSamplerE_1    = ESample1[4];
      float e_EME1_1   = ESample1[5];
      float e_EME2_1   = ESample1[6];
      float e_EME3_1   = ESample1[7];
      float e_EM_1   = e_PreSamplerB_1+e_EMB1_1+e_EMB2_1+e_EMB3_1+e_PreSamplerE_1+e_EME1_1+e_EME2_1+e_EME3_1;

      float e_PreSamplerB_2     = ESample2[0];
      float e_EMB1_2   = ESample2[1];
      float e_EMB2_2   = ESample2[2];
      float e_PreSamplerE_2    = ESample2[4];
      float e_EME1_2   = ESample2[5];
      float e_EME2_2   = ESample2[6];
      float e_EME3_2   = ESample2[7];
      float e_EM_2   = e_PreSamplerB_2+e_EMB1_2+e_EMB2_2+e_EMB3_2+e_PreSamplerE_2+e_EME1_2+e_EME2_2+e_EME3_2;


      float EMFrac1 = e_EM_1 / cl1->rawE();
      float EMFrac2 = e_EM_2 / cl2->rawE();


      double sigmaI = atan(sqrt(secondR1)/centerMag1)*cosh(cl1->eta());
      double sigmaJ = atan(sqrt(secondR2)/centerMag2)*cosh(cl2->eta());
      if ( cl1PJ.delta_R(cl2PJ) > sigmaI+sigmaJ)
        continue;

      if( !(EMFrac1 < 0.5 && EMFrac2 > 0.5) && !(EMFrac1 < 0.5 && EMFrac2 > 0.5))
        continue;

      //This condition basically asks if the clusters are overlapping and they both have energy in the first HAD layer or last EM layer (so are touching each other
      if( (e_EMB3_1 !=0 && e_EMB3_2 !=0) || (e_Tile0_1 !=0 && e_Tile0_2 !=0) || (e_EME3_1 !=0 && e_EME3_2 !=0) || (e_HEC0_1 !=0 && e_HEC0_2 !=0) ){
        already_merged[i] = true;
        already_merged[j] = true;

        fastjet::PseudoJet mergedConst = cl1PJ + cl2PJ;
        cl1->setE(mergedConst.e());
        cl1->setPhi(mergedConst.phi());
        cl1->setEta(mergedConst.eta());

        cl2->setE(0.);
      }
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode ClusterMergeHadEM::process(xAOD::IParticleContainer* cont) const {
  xAOD::CaloClusterContainer* clust = dynamic_cast<xAOD::CaloClusterContainer*> (cont); // Get CaloCluster container
  if(clust) return process(clust);
  return StatusCode::FAILURE;
}




