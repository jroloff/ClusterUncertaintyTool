'''
    Example python script to be run like

        xAH_run.py --files ... --config gamma_b.py ... <driver>
'''

from xAH_config import xAH_config
c = xAH_config()

c.setalg("BasicEventSelection", {"m_truthLevelOnly": False,
																 "m_name": "EventSelection",
                                 "m_applyGRLCut": False,
                                 "m_doPUreweighting": True,
                                 "m_vertexContainerName": "PrimaryVertices",
                                 "m_PVNTrack": 2,
                                 "m_useMetaData": False,
                                })

# Apply the cluster variations
c.setalg("ClusterUncertaintyTool", {"m_name": "ClusterUncertainty",
                                    "m_inContainerName": “CaloCalTopoClusters”,
                                   }
)


# Create R = 0.8 jets
jetName = "AntiKt8LCTopoJets"
c.setalg("JetMakerWithSystematics", {"m_name": "JetMaker2",
                                     "JetRadius": 0.8,
                                     "InputClusters": "CaloCalTopoClusters",
                                     "OutputContainer": "%s"%jetName
                                    }
        )



c.setalg("TreeAlgo", {"m_name": "OutputTree",
                      "m_fatJetContainerName": "m_fatJetContainerName": "AntiKt8LCTopoJets CE_AntiKt8LCTopoJets CER_AntiKt8LCTopoJets CESUp1_AntiKt8LCTopoJets CESDown1_AntiKt8LCTopoJets", 
                      "m_fatJetDetailStr": "kinematic energy scales",
                    })





