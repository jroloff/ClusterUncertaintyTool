# Cluster Uncertainties

This package implements tools to estimate bottom-up uncertainties for jets using three different sources of uncertainties.
It creates a new cluster collection for each variation, which can then be made into a new jet collection to use in estimating
the uncertainties. 

This package is compatible with xAODAnaHelpers, and everything can be setup as follows:


```
mkdir ClusterUncertainties

cd ClusterUncertainties

git clone https://github.com/UCATLAS/xAODAnaHelpers

git clone https://:@gitlab.cern.ch:8443/jroloff/ClusterUncertaintyTool.git

rcSetup Base,2.4.37

rc find_packages

rc compile

```

Then, this tool may be included along with any other xAODAnaHelpers tools in a config file.
An example config file may be seen in scripts/clusterUncertainties.py. This takes an input cluster collection (CaloCalTopoClusters) and feeds it
to the ClusterUncertaintyTool. This tool makes new cluster collections, which are then formed into jets using the JetMaker tool. All of the final jet collections
are then output in the resulting tree. 

For now, these uncertainties are only supported for LCTopo clusters, and the uncertainties would need to be rederived for any changes to this.

See also https://twiki.cern.ch/twiki/bin/view/Main/BottomUpUncerts for a detailed explanation of the different systematic uncertainties

## Creating Jet Collections

Two tools need to be included.

```
# Create all of the cluster collections
c.setalg("ClusterUncertaintyTool", {"m_name": "ClusterUncertainty",
                                    "m_inContainerName": "CaloCalTopoClusters"
                                   }
        )


# Create R = 0.8 jets
c.setalg("JetMakerWithSystematics", {"m_name": "JetMaker2",
                                     "JetRadius": 0.8,
                                     "InputClusters": "CaloCalTopoClusters",
                                     "OutputContainer": "AntiKt8LCTopoJets"
                                    }
        )



```


## Cluster Efficiency (CE):

The reconstruction efficiency of cluster is determined using the E/p of isolated clusters. 
Clusters are randomly dropped based on the energy of the cluster and the maximum difference between data and Monte Carlo.
These numbers are based on results from 2015 from low mu data.


## Cluster Angular Resolution (CAR):

The angular resolution is parameterized in terms of eta and energy of the clusters. Using the data-Monte Carlo difference, 
the clusters are smeared by around 5 mrad.


## Cluster Energy Scale (CES):

All clusters are varied up or down (CESUp, CESDown) coherently, with variations in E and eta based on results from E/p data and test beam results.

There are a number of cross checks on various ways of applying the CES. To deal with this, there are a number of different ways of running this cross-check.

## Cluster Energy Resolution

Clusters are varied by a Gaussian smearing, based on their eta and momentum. The amount of smearing is based on E/p data and test beam results.

## Cluster Splitting / Merging:

1. Splitting between HAD and EM (ClusterMergeHadEM)

2. Hadronic cluster splitting in the EM calorimeter (ClusterMergeEM)

3. Cluster splitting (ClusterSplitting)


## Uncertainties for hadrons which are not pions

This is not yet implemented.
