// this file is -*- C++ -*- 

#ifndef ClusterUncertaintyTool_ClusterMergeHadEM_H
#define ClusterUncertaintyTool_ClusterMergeHadEM_H




#include <string>
#include "JetRecTools/JetConstituentModifierBase.h"
#include "xAODBase/IParticleContainer.h"

#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "TRandom3.h"



class ClusterMergeHadEM : public JetConstituentModifierBase{
  ASG_TOOL_CLASS(ClusterMergeHadEM, IJetConstituentModifier)

  public:
  
  ClusterMergeHadEM(const std::string& name);
  StatusCode process(xAOD::IParticleContainer* cont) const; 
  StatusCode process(xAOD::CaloClusterContainer* cont) const; // MEN: Might need to rename this process


private:

 TRandom3* myrand_global; //!

		
};


#endif
