// this file is -*- C++ -*- 
// ClusterUncertaintyTool.h

#ifndef ClusterUncertaintyTool_CES_H
#define ClusterUncertaintyTool_CES_H


#include <string>
#include "JetRecTools/JetConstituentModifierBase.h"
#include "xAODBase/IParticleContainer.h"

#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "TRandom3.h"



class ClusterEnergyScale : public JetConstituentModifierBase{
  ASG_TOOL_CLASS(ClusterEnergyScale, IJetConstituentModifier)

  public:
  
  ClusterEnergyScale(const std::string& name);
  StatusCode process(xAOD::IParticleContainer* cont) const; 
  StatusCode process(xAOD::CaloClusterContainer* cont) const; // MEN: Might need to rename this process

  enum CESName {
    none                 = 0,
    EMFracLow            = 1,
    EMFracHigh           = 2,
    EMProb               = 3,
    EtaForward           = 4,
    EtaCentral           = 5,
    OutOfTestRange       = 6,
    InTestRange          = 7,
    PtLow                = 8,
    PtHigh               = 9,
  };



  //CESName m_type;
  int m_type;
  std::string m_method;

private:

 TRandom3* myrand_global; //!

  double getCES_EMFracLow(xAOD::CaloCluster* cluster, double CES) const;

  double getCES_EMFracHigh(xAOD::CaloCluster* cluster, double CES) const;

  double getCES_EMProb(xAOD::CaloCluster* cluster, double CES) const;
  double getCES_OutOfTestRange(xAOD::CaloCluster* cluster, double CES) const;
  double getCES_InTestRange(xAOD::CaloCluster* cluster, double CES) const;

  double getCES_EtaForward(xAOD::CaloCluster* cluster, double CES) const;

  double getCES_EtaCentral(xAOD::CaloCluster* cluster, double CES) const;
  double getCES_PtLow(xAOD::CaloCluster* cluster, double CES) const;

  double getCES_PtHigh(xAOD::CaloCluster* cluster, double CES) const;




};


#endif
