// this file is -*- C++ -*- 
// SoftKillerWeightTool.h

#ifndef ClusterUncertaintyTool_CAR_H
#define ClusterUncertaintyTool_CAR_H


#include <string>
#include "JetRecTools/JetConstituentModifierBase.h"
#include "xAODBase/IParticleContainer.h"

#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"



class ClusterAngularResolution : public JetConstituentModifierBase{
  ASG_TOOL_CLASS(ClusterAngularResolution, IJetConstituentModifier)

  public:
  
  ClusterAngularResolution(const std::string& name);
  StatusCode process(xAOD::IParticleContainer* cont) const; 
  StatusCode process(xAOD::CaloClusterContainer* cont) const; // MEN: Might need to rename this process


private:

 TRandom3* myrand_global; //!

		
};


#endif
