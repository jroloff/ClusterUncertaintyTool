// this file is -*- C++ -*- 

#ifndef ClusterUncertaintyTool_CE_H
#define ClusterUncertaintyTool_CE_H




#include <string>
#include "JetRecTools/JetConstituentModifierBase.h"
#include "xAODBase/IParticleContainer.h"

#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "TRandom3.h"



class ClusterEfficiency : public JetConstituentModifierBase{
  ASG_TOOL_CLASS(ClusterEfficiency, IJetConstituentModifier)

  public:
  
  ClusterEfficiency(const std::string& name);
  StatusCode process(xAOD::IParticleContainer* cont) const; 
  StatusCode process(xAOD::CaloClusterContainer* cont) const; // MEN: Might need to rename this process


private:

 TRandom3* myrand_global; //!

		
};


#endif
