// this file is -*- C++ -*- 
// ClusterUncertaintyTool.h

#ifndef ClusterUncertaintyTool_CER_H
#define ClusterUncertaintyTool_CER_H


#include <string>
#include "JetRecTools/JetConstituentModifierBase.h"
#include "xAODBase/IParticleContainer.h"

#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/Selector.hh"

#include "TRandom3.h"



class ClusterEnergyResolution : public JetConstituentModifierBase{
  ASG_TOOL_CLASS(ClusterEnergyResolution, IJetConstituentModifier)

  public:
  
  ClusterEnergyResolution(const std::string& name);
  StatusCode process(xAOD::IParticleContainer* cont) const; 
  StatusCode process(xAOD::CaloClusterContainer* cont) const; // MEN: Might need to rename this process

	std::string m_method;

private:

 TRandom3* myrand_global; //!

		
};


#endif
